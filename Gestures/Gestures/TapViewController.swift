//
//  TapViewController.swift
//  Gestures
//
//  Created by Robert on 17/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var LabelTouche: UILabel!
    
    @IBOutlet weak var CustomView: UIView!
    @IBOutlet weak var CoordLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        self.LabelTouche.text = "\(touchCount ?? 0)"
        self.tapLabel.text = "\(tapCount ?? 0)"
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        //print("X: \(x), Y: \(y)")
        self.CoordLabel.text = "(\(x!) , \(y!))"
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("termind")
    }

    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        
        /*var taps = 0
        if sender.state == .ended{
            taps += 1
            self.tapLabel.text = "\(taps)"
        }*/
        
        CustomView.backgroundColor = .red
    }
    
}
