//
//  StoryboardsTabBarController.swift
//  Gestures
//
//  Created by Robert on 23/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class StoryboardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        left.image = #imageLiteral(resourceName: "ic_launch")
        right.image = #imageLiteral(resourceName: "ic_input")
        
        
        left.title = "Tab"
        right.title = "Swipe"
        
        left.badgeColor = .purple
        right.badgeColor = .red
    }

}
