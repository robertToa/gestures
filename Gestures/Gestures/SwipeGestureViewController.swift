//
//  SwipeGestureViewController.swift
//  Gestures
//
//  Created by Robert on 17/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func swipeUpAction(_ sender: Any) {
        
        customView.backgroundColor = .black
    }
    
    @IBAction func swipeDownAction(_ sender: Any) {
        customView.backgroundColor = .purple
    }
    
    
    @IBAction func swipeLeftAction(_ sender: Any) {
        customView.backgroundColor = .red
    }
    
    @IBAction func swipeRightAction(_ sender: Any) {
        customView.backgroundColor = .green
    }
    
}
